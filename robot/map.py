"""
@author: Lucas Magnac & Teddy Aulas
"""
from pickle import NEWOBJ_EX
import numpy as np
import random
import tkinter as tk

""" Cette classe représente un robot qui se déplace dans une grille"""


class Grid:

    def __init__(self, nbr_lignes, nbr_colonnes):
        self.nbr_lignes = nbr_lignes
        self.nbr_colonnes = nbr_colonnes
        self.grid = []
        self.index = [2]
        for i in range((self.nbr_lignes)):
            self.grid.append([])
            for j in range((self.nbr_colonnes)):
                self.grid[i].append(0)
                j = j + 1
            i = i + 1
        self.grid[0][0] = 1

    def creation_grid(self):

        """Cette méthode permet d'afficher la grille ainsi que le robot."""

        for i in range((self.nbr_lignes)): 
            print("|", end='')
            for j in range((self.nbr_colonnes)):  
                print(self.grid[i][j], end='')
            print("|")
        print("")
        return True

    def creation_obstacle(self):

        "Cette méthode permet d'afficher des obstacles sur la grille"

        self.nbr_places = self.nbr_lignes * self.nbr_colonnes - 1
        self.nbr_obj = random.randint(0,self.nbr_places) 
        for i in range(1):
            self.ord = 1
            self.abs = 1
            self.grid[2,2]=2
            if self.grid[self.ord][self.abs] == 0:
                self.grid[self.ord][self.abs] = 2
                self.nbr_obj = self.nbr_obj-1    

    def go_up(self):

        """Cette méthode permet au robot d'aller vers le haut si c'est possible pour lui"""

        for i in self.grid:
            if 1 in i:
                if self.grid.index(i) - 1 >= 0:
                    stock = self.grid[self.grid.index(i) - 1]
                    self.grid[self.grid.index(i) - 1] = self.grid[self.grid.index(i)]
                    self.grid[self.grid.index(i) + 1] = stock
                    return True
        return False

    def down(self):

        """Cette méthode permet au robot d'aller vers le bas si c'est possible pour lui"""

        for i in self.grid:
            if 1 in i:
                if self.grid.index(i) + 1 < len(self.grid):
                    stock = self.grid[self.grid.index(i) + 1]
                    self.grid[self.grid.index(i) + 1] = self.grid[self.grid.index(i)]
                    self.grid[self.grid.index(i)] = stock
                    return True
        return False

    def right(self):

        """Cette méthode permet au robot d'aller vers la droite si c'est possible pour lui"""

        for i in self.grid:
            if 1 in i:
                if i.index(1) + 1 < len(i):
                    stock = i[i.index(1) + 1]
                    i[i.index(1) + 1] = i[i.index(1)]
                    i[i.index(1)] = stock
                    return True
        return False

    def left(self):

        """Cette méthode permet au robot d'aller vers la gauche si c'est possible pour lui"""

        for i in self.grid:
            if 1 in i:
                if i.index(1) - 1 >= 0:
                    stock = i[i.index(1) - 1]
                    i[i.index(1) - 1] = i[i.index(1)]
                    i[i.index(1) + 1] = stock
                    return True
        return False
