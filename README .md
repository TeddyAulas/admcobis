# Projet_AdmCo_robot

Notre projet comporte 3 classes, localisées dans le dossier Robot :
- map.py, la classe primaire;
- jeuclavier.py, permetant l'utilisation du clavier pour le jeu;
- itfGraphique.py, classe de rendu graphique sur turtle.

Pour essayer le jeu du robot, vous pouvez lancer les deux derniers scripts sous python3.
L'interface Homme/Machine a été fini.
Les extensions graphiques ne sont pas finies, mais sont en cours de fiitions ( et seront rendues au moment du deuxième dépôt).
Pour ce qui s'agit des fichiers de test, on vous prévient que les deux premières classes sont complètes et fonctionnelles, en revanche, celui du rendu graphique n'a pas pu être terminé.

## I) Les objectifs

Le but de ce projet est de réaliser un genre de jeu, dans lequel un robot doit se déplacer sur une grille remplie d'obstacles.

## II) Organigramme 

 .
    ├── README.md
    ├── Robot
    │   ├── __init__.py
    │   ├── itfgraphique.py
    │   ├── jeuclavier.py
    │   ├── map.py
    └── test
        ├── clavier_test.py
        ├── graphique_test.py
        ├── __init__.py
        └── robot_test.py

(3 directories, 17 files)


## III) Installation package


Pour pouvoir utiliser les package ensemble, l'import de package (et de leur contenu) est nécessaire.
Voici les différentes méthodes d'installation de packages :

1. PYTHONPATH
La première solution, relativement contraignante mais rapide à mettre en place, et de placer le PYTHONPATH à la racine du projet, pour cela il faut ouvrir un terminal et se placer à la racine du projet et entrez la commande suivante :

    export PYTHONPATH=$PYTHONPATH:'.'

Vous pouvez ensuite appeler les scripts d'un package à l'extérieur de celui-ci (à condition de bien renseigner le chemin vers ce script bien évidemment).


2. SETUP.PY

Source : https://packaging.python.org/tutorials/packaging-projects/#packaging-your-project

La deuxième solution est de créer un script python setup.py qui va permettre d'importer un package. Pour cela il faut dans ce script indiquer le chemin d'accès à ce Package. Il faudra aussi dans certains cas définir un fichier texte requirements permettant de spécifier certaines versions de modules nécessaires à la bonne réalisation des actions. Il faut taper les commandes suivantes : 

"python3 setup.py sdist bdist_wheel"

En cas de message d'erreur, il faut créer un environnement virtuel et installer le fichier requirements : 

    sudo apt-get install python3-venv

    python3 -m venv venv                    #création du virtual env

    source ./venv/bin/activate              #activation du virtual env

    pip install -r requirements.txt

    python3 setup.py sdist bdist_wheel

Pour sortir de l'environnement virtuel, il suffit de rentrer la commande : "desactivate".

3. Test.PyPi

La dernière méthode pour installer et importer un package est de passer par la plateforme TestPypi. Pour cela, il faut créer un compte, créér comme précédemment un fichier setup.py, et importer via internet le package souhaité. Une fois le token créé sur le site de Pypi voici les différentes commandes à entrer :

    python3 -m pip install --user --upgrade setuptools wheel

    python3 setup.py sdist bdist_wheel

    python3 -m pip install --user --upgrade twine

    python3 -m twine upload --repository testpypi dist/*

Seront alors demandés un username et un mdp, le login est __token__ et le mdp est donnée sur le site.


## IV) Réalisation des tests

Après avoir importé et installé les packages, on peut enfin faire des tests. Voici les différentes possibilités qui nous sont proposées :

1. Exécution du fichier test
L'appel directement du sccript peut être effectué, mais il faut cependant avoir préalablement réglé le PYTHONPATH. Se placer a la racine du projet dans un terminal, et entrer la commande suivante :


    python3 <chemin complet du fichier test>

2. Pytest 

La seconde méthode est d'entrer la commande pytest à la racine du projet. Cette commande va exécuter tous les scripts python qui possède "test" dans leur nom.

On peut voir uniquement dans ce cas là le nombre de tests effectués, le pourcentage de réussite et le temps que le programme a mis pour exécuter les tests.
