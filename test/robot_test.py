"""
@author: Lucas Magnac & Teddy Aulas
"""

import unittest
import logging
from robot.map import Grid


class TestPrint(unittest.TestCase):

    """Cette classe est une classe test pour afficher"""

    def setUp(self):

        """Méthode pour mettre en place la grille """

        self.map = Grid(3, 3)
        self.log = logging.getLogger()

    def test_print(self):

        """Cette méthode nous permet de vérifier si on a bien le résultat attendu"""

        self.assertEqual(self.map.creation_grid(), True)
        self.log.warning("Affichage de la map : ok ")

class TestUp(unittest.TestCase):

    """Classe qui permet de tester si le robot va bien en haut"""

    def setUp(self):

        """Méthode pour mettre en place la grille """

        self.map = Grid(3, 3)
        self.log = logging.getLogger()

    def test_up(self):
        
        """Cette méthode nous permet de vérifier si on a bien le résultat attendu"""
        
        self.assertEqual(self.map.go_up(), False)
        self.log.warning("Test collision haut : OK")
        self.map.down()
        self.assertEqual(self.map.go_up(), True)
        self.log.warning("Aller en haut : OK")

class TestDown(unittest.TestCase):

    """Classe qui permet de tester si le robot va bien en bas"""

    def setUp(self):

        """Méthode pour mettre en place la grille """
        
        self.map = Grid(3, 3)
        self.log = logging.getLogger()

    def test_down(self):

        """Cette méthode nous permet de vérifier si on a bien le résultat attendu"""

        self.assertEqual(self.map.down(), True)
        self.log.warning("Aller en bas : OK")
        self.assertEqual(self.map.down(), True)
        self.assertEqual(self.map.down(), False)
        self.log.warning("Test collision bas : OK")

class TestRight(unittest.TestCase):

    """Classe qui permet de tester si le robot va bien à droite"""

    def setUp(self):

        """Méthode pour mettre en place la grille """

        self.map = Grid(3, 2)
        self.log = logging.getLogger()

    def test_right(self):

        """Cette méthode nous permet de vérifier si on a bien le résultat attendu"""

        self.assertEqual(self.map.right(), True)
        self.log.warning("Aller à droite : OK")
        self.assertEqual(self.map.right(), False)
        self.log.warning("Test collision droite : OK")

class TestLeft(unittest.TestCase):

    """Classe qui permet de tester si le robot va bien à gauche"""

    def setUp(self):

        """Méthode pour mettre en place la grille """

        self.map = Grid(3, 3)
        self.log = logging.getLogger()

    def test_left(self):

        """Cette méthode nous permet de vérifier si on a bien le résultat attendu"""

        self.map.right()
        self.assertEqual(self.map.left(), True)
        self.log.warning("Aller à gauche : OK")
        self.assertEqual(self.map.left(), False)
        self.log.warning("Test collision gauche : OK")


if __name__ == "__main__":
    unittest.main()
